* clipfrme.s
*
* Program for chapter 4
* A program to clip and fill apolygon to a window (clip fram)
* defined by the limits clp_xmin, clp_xmax, clp_ymin, clp_ymax
*

  *SECTION TEXT
  opt     d+          incldue levels for debugging
  bra     main        don't execute the includes
  inlude  equates.    constants
  inlude  systm_00.s  housekeeping
  include core_01.s   important subroutines

main  bsr set_up    screens, copper, blitter, etc

blit_loop:
      bsr drw_shw2
      move.w  #12-1,d7  six pari of coords for vertices
      lea     crds_in,a0    destination
      move.l  a0,a3         ready for drawing
      lea     my_data,a1    from here

clp_loop
      move.w  (a1)+,(a0)+   transfer
      dbf     d7,clp_loop   them all

      move.w  #5,no_in    5 sides to the polygon
      move.w  my_colour,colour      set the colour
      move.w  my_xmin,clp_xmin      set the 
      move.w  my_max,clp_xmax       clip
      move.w  my_ymin,clp_ymin      fram
      move.w  my_ymax,clp_ymax      limits

      bsr     drw2_shw1             draw on screen 2, display 1
      bsr     clip                  window it
      bsr     poly_fill             fill it
      bsr     drw1_shw2             show the drawing

loop_again:
      bra     loop_again            forever

*SECTION DATA
* A pentagon
my_data       dc.w    20,100,200,20,300,80,260,180,140,180,20,100
* which is pink
my_colour     dc.w    24

* The window limits
my_xmin       dc.w    50
my_xmax       dc.w    270
my_ymin       dc.w    50  
my_ymax       dc.w    150

*SECTION BSS
  include bss_01.s

* SECTION DATA
  include data_00.s

END

